import { Component, Input, OnInit, ɵSWITCH_CHANGE_DETECTOR_REF_FACTORY__POST_R3__ } from '@angular/core';
import { LegendItem, ChartType } from '../lbd/lbd-chart/lbd-chart.component';
import * as Chartist from 'chartist';
import { ChartDataSets, ChartOptions, Point, TimeScale } from 'chart.js';
import { DataService } from 'app/Services/data.service';
import { Label, Color } from 'ng2-charts';
import Chart = require('chart.js');
import { NamespaceService } from 'app/Services/Namespace.service';
import { collapseTextChangeRangesAcrossMultipleVersions } from 'typescript';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public Results = [];
  test1: any = []
  test: any[] = []
  public labelsArray: any[] = [];
  public seriesArray: any[] = [];
  public labelsArray1: any[] = [];
  public seriesArray1: any[] = [];
  public chart: any = [];
  selected: string;
  public collection: any[] = [];
  constructor(private DataService: DataService, private NamespaceService: NamespaceService) { }




  ngOnInit() {
    this.NamespaceService.getnamespace_info().subscribe(
      res => {

        for (let i = 0; i <= res['data']['result'].length - 1; i++) {

          this.collection.push(res['data']['result'][i]['metric'])
        }
      })

    ////////////////////////////////////////////////////////////////////////////////////////



    this.DataService.getnode_memory().subscribe(
      res => {

        for (let i = 0; i <= res['data']['result'].length - 1; i++) {

          this.Results.push(res['data']['result'][i]['values'])
        }
        this.Results.forEach(value => {
          // console.log("foreach ligne =======", value);
          value.forEach(val => {
            // get time  
            let unix_timestamp = val[0]
            let date = new Date(unix_timestamp * 1000);
            // Hours part from the timestamp
            let hours = date.getHours();
            // Minutes part from the timestamp
            let minutes = "0" + date.getMinutes();
            // Seconds part from the timestamp
            let seconds = "0" + date.getSeconds();

            // Will display time in 10:30:23 format
            let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

            // end get time 
            if (val[1] === 0) return '0 Bytes';
            const k = 1024;
            const dm = 2;
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            const i = Math.floor(Math.log(val[1]) / Math.log(k));

            let node_cpu = parseFloat((val[1] / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];

            this.labelsArray1.push(formattedTime);
            this.seriesArray1.push(val[1]);

            //  this.SeriesArray.push(node_cpu);         
          });
        });
        ////////////////////////////// Chart Memory 
        this.chart = new Chart('ram', {
          // The type of chart we want to create
          type: 'line',

          // The data for our dataset
          data: {
            labels: this.labelsArray1,
            datasets: [{
              label: 'dataset',
              backgroundColor: 'rgb(255, 99, 132)',
              borderColor: 'rgb(255, 99, 132)',
              data: this.seriesArray1
            }]
          },

          // Configuration options go here
          options: {
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true
              }],
              yAxes: [{
                display: true,
                ticks: {
                  beginAtZero: false
                },
                scaleLabel: {
                  display: true,
                  labelString: 'RAM usage'
                },
                afterTickToLabelConversion: function (q) {
                  for (var tick in q.ticks) {
                    q.ticks[tick] += ' Mo';
                  }
                }
              }]
            }
          }

        });

      })

    ////////////////////////////////////////////////////////////////////////////////////////

    this.DataService.getnode_cpu().subscribe(
      res => {

        for (let i = 0; i <= res['data']['result'].length - 1; i++) {
          this.test.push(res['data']['result'][i]['values'])
          this.test1.push(res['data']['result'][i]['metric']['pod'])
        }


        this.test.forEach(value => {
          value.forEach(val => {
            // get time  
            let unix_timestamp = val[0]

            let date = new Date(unix_timestamp * 1000);
            // Hours part from the timestamp
            let hours = date.getHours();
            // Minutes part from the timestamp
            let minutes = "0" + date.getMinutes();
            // Seconds part from the timestamp
            let seconds = "0" + date.getSeconds();

            // Will display time in 10:30:23 format
            let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);


            // end get time 
            if (val[1] === 0) return '0 Bytes';
            const k = 1024;
            const dm = 2;
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            const i = Math.floor(Math.log(val[1]) / Math.log(k));

            let node_cpu = parseFloat((val[1] / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];

            this.labelsArray.push(formattedTime);
            this.seriesArray.push(val[1]);


            //  this.SeriesArray.push(node_cpu);         
          });
        });
        //////////////////////// Chart CPU
        this.chart = new Chart('canvas', {
          // The type of chart we want to create
          type: 'line',

          // The data for our dataset
          data: {
            labels: this.labelsArray,
            datasets: [{
              label: this.test1,
              backgroundColor: 'rgb(255, 99, 132)',
              borderColor: 'rgb(255, 99, 132)',
              data: this.seriesArray
            }]
          },

          // Configuration options go here
          options: {
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true,

              }],
              yAxes: [{
                display: true,
                ticks: {
                  beginAtZero: false
                },
                scaleLabel: {
                  display: true,
                  labelString: 'CPU usage'
                },
                afterTickToLabelConversion: function (q) {
                  for (var tick in q.ticks) {
                    q.ticks[tick] += ' CPU';
                  }
                }
              }]
            }
          }

        });

      })

  }

  applyDateFilter(v) {
    this.DataService.getnode_memory().subscribe(res => {
      let labelsArray1 = []
      let seriesArray1 = []
      let results
      res['data'].result.forEach(value => {

        if (value.metric.namespace === v) {

          results = value.values

          results.forEach(val => {
            // get time  
            let unix_timestamp = val[0]
            let date = new Date(unix_timestamp * 1000);
            // Hours part from the timestamp
            let hours = date.getHours();
            // Minutes part from the timestamp
            let minutes = "0" + date.getMinutes();
            // Seconds part from the timestamp
            let seconds = "0" + date.getSeconds();


            // Will display time in 10:30:23 format
            let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

            // end get time 
            if (val[1] === 0) return '0 Bytes';
            const k = 1024;
            const dm = 2;
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            const i = Math.floor(Math.log(val[1]) / Math.log(k));


            labelsArray1.push(formattedTime);
            seriesArray1.push(val[1]);

            //  this.SeriesArray.push(node_cpu);         
          });


          this.chart = new Chart('ram', {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
              labels: labelsArray1,
              datasets: [{
                label: this.test1,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: seriesArray1
              }]
            },

            // Configuration options go here
            options: {
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  display: true,

                }],
                yAxes: [{
                  display: true,
                  ticks: {
                    beginAtZero: false
                  },
                  scaleLabel: {
                    display: true,
                    labelString: 'RAM usage'
                  },
                  afterTickToLabelConversion: function (q) {
                    for (var tick in q.ticks) {
                      q.ticks[tick] += ' Mo';
                    }
                  }
                }]
              }
            }

          });
          this.chart.update()
        }


      })
    })

    this.DataService.getnode_cpu().subscribe(res => {
      let labelsArray1 = []
      let seriesArray1: any[] = []
      let results
      res['data'].result.forEach(value => {

        if (value.metric.namespace === v) {

          results = value.values

          results.forEach(val => {
            // get time  
            let unix_timestamp = val[0]
            let date = new Date(unix_timestamp * 1000);
            // Hours part from the timestamp
            let hours = date.getHours();
            // Minutes part from the timestamp
            let minutes = "0" + date.getMinutes();
            // Seconds part from the timestamp
            let seconds = "0" + date.getSeconds();


            // Will display time in 10:30:23 format
            let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

            // end get time 
            if (val[1] === 0) return '0 Bytes';
            const k = 1024;
            const dm = 2;
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            const i = Math.floor(Math.log(val[1]) / Math.log(k));


            labelsArray1.push(formattedTime);
            seriesArray1.push(val[1]);

            //  this.SeriesArray.push(node_cpu);         
          });


          this.chart = new Chart('canvas', {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
              labels: labelsArray1,
              datasets: [{
                label: this.test1,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: seriesArray1
              }]
            },

            // Configuration options go here
            options: {
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  display: true,

                }],
                yAxes: [{
                  display: true,
                  ticks: {
                    beginAtZero: false
                  },
                  scaleLabel: {
                    display: true,
                    labelString: 'CPU usage'
                  },
                  afterTickToLabelConversion: function (q) {
                    for (var tick in q.ticks) {
                      q.ticks[tick] += ' CPU';
                    }
                  }
                }]
              }
            }

          });
          this.chart.update()
        }


      })

    }

    )

  }
}


