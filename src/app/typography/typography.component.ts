import { Component, OnInit } from '@angular/core';
import Chart = require('chart.js');
import { DataService } from 'app/Services/data.service';
import { NamespaceService } from 'app/Services/Namespace.service';

@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.css']
})
export class TypographyComponent implements OnInit {
  public chart: any = [];
  public test: any[] = [];
  public test1 = [];
  public labelsArray: any[] = []
  public seriesArray: any[] = []
  public collection: any[] = [];
  public price = 0.202
  public nbrCPU = 4
  public nbrRAM = 16
  public aksCPUprices
  public aksRamprices
  constructor(private DataService: DataService, private NamespaceService: NamespaceService) { }

  ngOnInit() {
    this.NamespaceService.getnamespace_info().subscribe(
      res => {

        for (let i = 0; i <= res['data']['result'].length - 1; i++) {

          this.collection.push(res['data']['result'][i]['metric'])
        }
      })
      this.CpuPricePerUnit()   
  }

  CpuPricePerUnit(){   
    let CPUprice     
    CPUprice = this.price / (this.nbrRAM/3)
    let RAMprice = CPUprice / 3
     this.aksCPUprices = CPUprice ? CPUprice  : '' 
     this.aksRamprices = RAMprice ? RAMprice : ''
  }
  


  applyDateFilter(v) {
    this.DataService.getnode_cpu().subscribe(res => {
      let labelsArray1 = []
      let seriesArray1: any[] = []
      let results
      res['data'].result.forEach(value => {

        if (value.metric.namespace === v) {

          results = value.values

          results.forEach(val => {
            // get time  
            let unix_timestamp = val[0]
            let date = new Date(unix_timestamp * 1000);
            // Hours part from the timestamp
            let hours = date.getHours();
            // Minutes part from the timestamp
            let minutes = "0" + date.getMinutes();
            // Seconds part from the timestamp
            let seconds = "0" + date.getSeconds();


            // Will display time in 10:30:23 format
            let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

            // end get time 
            if (val[1] === 0) return '0 Bytes';
            const k = 1024;
            const dm = 2;
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            const i = Math.floor(Math.log(val[1]) / Math.log(k));


            labelsArray1.push(formattedTime);
            seriesArray1.push(val[1]);

            //  this.SeriesArray.push(node_cpu);         
          });
          
          let output = seriesArray1.map(x => x * this.aksCPUprices)
          console.log(output)
          this.chart = new Chart('price', {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
              labels: labelsArray1,
              datasets: [{
                label: 'price',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: output
              }]
            },

            // Configuration options go here
            options: {
              tooltips: {
                callbacks: {
                  label: (tooltipItems, data) => {
                    return data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index] + '  €';
                  }
                },
              },
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  display: true,

                }],
                yAxes: [{
                  display: true,
                  ticks: {
                    beginAtZero: false
                  },
                  scaleLabel: {
                    display: true,
                    labelString: 'CPU pricing'
                  },
                  afterTickToLabelConversion: function (q) {
                    for (var tick in q.ticks) {
                      q.ticks[tick] += ' €';
                    }
                  }
                }]
              }
            }

          });
          this.chart.update()
        }
      })

    })

    this.DataService.getnode_memory().subscribe(res => {
      let labelsArray1 = []
      let seriesArray1 = []
      let results
      res['data'].result.forEach(value => {

        if (value.metric.namespace === v) {

          results = value.values

          results.forEach(val => {
            // get time  
            let unix_timestamp = val[0]
            let date = new Date(unix_timestamp * 1000);
            // Hours part from the timestamp
            let hours = date.getHours();
            // Minutes part from the timestamp
            let minutes = "0" + date.getMinutes();
            // Seconds part from the timestamp
            let seconds = "0" + date.getSeconds();


            // Will display time in 10:30:23 format
            let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

            // end get time 
            if (val[1] === 0) return '0 Bytes';
            const k = 1024;
            const dm = 2;
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            const i = Math.floor(Math.log(val[1]) / Math.log(k));

            let node_cpu = parseFloat((val[1] / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];

            labelsArray1.push(formattedTime);
            seriesArray1.push(val[1]);
            
             //seriesArray1.push(node_cpu);  
                   
          });

          
          let output = seriesArray1.map(x => x * this.aksRamprices)
          
          this.chart = new Chart('priceRam', {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
              labels: labelsArray1,
              datasets: [{
                label: 'price',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: output
              }]
            },

            // Configuration options go here
            options: {
              tooltips: {
                callbacks: {
                  label: (tooltipItems, data) => {
                    return data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index] + '  €';
                  }
                },
              },
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  display: true,

                }],
                yAxes: [{
                  display: true,
                  ticks: {
                    beginAtZero: false
                  },
                  scaleLabel: {
                    display: true,
                    labelString: 'RAM pricing'
                  },
                 afterTickToLabelConversion: function (q) {
                    for (var tick in q.ticks) {
                      q.ticks[tick] += ' €';
                    }
                  }
                }]
              }
            }
          });
          this.chart.update()
        }
      })
    })

  }
}
