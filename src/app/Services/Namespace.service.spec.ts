/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NamespaceService } from './Namespace.service';

describe('Service: Namespace', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NamespaceService]
    });
  });

  it('should ...', inject([NamespaceService], (service: NamespaceService) => {
    expect(service).toBeTruthy();
  }));
});
