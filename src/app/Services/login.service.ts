import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public  start =''
  public end =''
  
  constructor(private httpClient:HttpClient) { }
  
  private handleError(err: HttpErrorResponse) {

    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {

      errorMessage = `An error occurred: ${err.error.message}`;
    } else {

      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
  getnode_info() {
    return this.httpClient.get<[]>('https://alertmanager.accretio.io/api/v1/query_range?query=kube_node_labels&start=1619521508.954&end=1619525108.954&step=14&_=1619525047372').pipe(data=>data, catchError(this.handleError))
  }
}
