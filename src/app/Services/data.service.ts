import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public  start =''
  public end =''
  public host = `https://alertmanager.accretio.io/api/v1/query_range?query=namespace:container_cpu_usage_seconds_total:sum_rate&start=1618560829.852&end=1618564429.852&step=14&_=1618560754002`;
  public host1='https://alertmanager.accretio.io/api/v1/query_range?query=sum(container_memory_usage_bytes{container_name!="POD",container_name!=""}) by (namespace)&start=1617199148.991&end=1617202748.991&step=14&_=1617202247419';
  public host2 = 'https://alertmanager.accretio.io/api/v1/query_range?query=node_filesystem_size_bytes {job="node-exporter"} - node_filesystem_avail_bytes {job="node-exporter"}&start=1619518318.467&end=1619521918.467&step=14&_=1619520299459'
  constructor(private httpClient:HttpClient) { }
  
  private handleError(err: HttpErrorResponse) {

    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {

      errorMessage = `An error occurred: ${err.error.message}`;
    } else {

      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
  getnode_cpu() {
    return this.httpClient.get<[]>(this.host).pipe(data=>data, catchError(this.handleError))
  }
  getnode_memory() {
    return this.httpClient.get<[]>(this.host1).pipe(data=>data, catchError(this.handleError))
  }
  getUsed_memory(){
    return this.httpClient.get<[]>(this.host2).pipe(data=>data, catchError(this.handleError))
  }

}
