import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NamespaceService {
  public  start =''
  public end =''
  public host = `https://alertmanager.accretio.io/api/v1/query?query=kube_namespace_labels&time=1617797811.837&_=1617797747955`;
  constructor(private httpClient:HttpClient) { }
  
  private handleError(err: HttpErrorResponse) {

    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {

      errorMessage = `An error occurred: ${err.error.message}`;
    } else {

      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
  getnamespace_info() {
    return this.httpClient.get<[]>(this.host).pipe(data=>data, catchError(this.handleError))
  }
}

