import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public pricess;
  public total;
  public prices
  public price = 145.72;
  public discount
  public discountedPrice
  public total1
  public licensePrice = 111.72
  public aksPrice = 145.72
  public aksprices
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.PayAsYouGo()
    this.License()
    this.Total()
    this.PayAsYouGoAks()
  }
  PayAsYouGo() {
    this.price
    this.aksPrice
    
    this.prices = this.price ? this.price : ''

  }
  ApplyDiscount36() {

    this.discount = (this.price * 36) / 100
    this.discountedPrice = this.price - this.discount   
    this.prices = this.discountedPrice ? this.discountedPrice : ''

  }
  ApplyDiscount56() {

    this.discount = (this.price * 56) / 100
    this.discountedPrice = this.price - this.discount   
    this.prices = this.discountedPrice ? this.discountedPrice : ''

  }
  License() {
    this.licensePrice
    this.pricess = this.licensePrice ? this.licensePrice : ''
  }
  AzureHybridBenefit() {
    this.pricess = 0
  }
  Total() {
    this.total = this.prices + this.pricess
    this.total1 = this.total ? this.total : ''
    console.log(this.total1)

  }
  PayAsYouGoAks() {
    this.aksPrice
    this.aksprices = this.aksPrice ? this.aksPrice : ''
  }

  ApplyDiscount36Aks() {

    this.discount = (this.price * 36) / 100
    this.discountedPrice = this.aksPrice - this.discount
    console.log(this.discountedPrice)
    this.aksprices = this.discountedPrice ? this.discountedPrice : ''

  }
  ApplyDiscount56Aks() {

    this.discount = (this.price * 56) / 100
    this.discountedPrice = this.aksPrice - this.discount
    console.log(this.discountedPrice)
    this.aksprices = this.discountedPrice ? this.discountedPrice : ''

  }
}
