import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/Services/login.service';
import Chart = require('chart.js');

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}
declare var $: any;
@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {
    public tableData1: TableData;
    public tableData2: TableData;
    public tableData3: TableData;
    public tableData4: TableData;
    public test1: any[] = []
    public namespace: any[] = []
    public name: any[] = []
    public chart: any = []

    constructor(private LoginService: LoginService) { }


    ngOnInit() {


        this.tableData1 = {
            headerRow: ['name', 'endpoint', 'instance', 'job', 'label_agentpool', 'label_beta_kubernetes_io_arch', 'label_beta_kubernetes_io_instance_type', 'label_beta_kubernetes_io_os'],
            dataRows:
                []
        };
        this.LoginService.getnode_info().subscribe(
            res => {

                for (let i = 0; i <= res['data']['result']?.length - 1; i++) {

                    //console.log(res['data']['result'][i]['metric'])
                    this.test1.push(res['data']['result'][i]['metric'])
                }
            })
        this.tableData1.dataRows = this.test1

        this.tableData2 = {
            headerRow: ['number of virtuel machines', 'Virtual machine price per hour', 'Compute plan', 'Estimated monthly price'],
            dataRows:
                [
                    ['1', '0.202 €/hr', 'Pay as you go', '145.72€']
                ]
        };

        this.tableData3 = {
            headerRow: ['Aks node pricing per hour', 'saving option', 'Estimated monthly price'],
            dataRows:
                [
                    ['0.202 €/hr', 'pay as you go', '147.75€']
                ]
        };
        this.tableData4 = {
            headerRow: ['estimated mounthly cost'],
            dataRows:
                [
                    ['291.49€']
                ]
        };
    }

    private initTable() {
        var table = $('#datatables').tableData1({
            "pagingType": "full_numbers",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }
        });
    }
    private initTable1() {
        var table = $('#datatables1').tableData2({
            "pagingType": "full_numbers",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }
        });
    }
    private initTable3() {
        var table = $('#datatables2').tableData3({
            "pagingType": "full_numbers",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }
        });
    }
    private initTable4() {
        var table = $('#datatables').tableData4({
            "pagingType": "full_numbers",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }
        });
    }
}
